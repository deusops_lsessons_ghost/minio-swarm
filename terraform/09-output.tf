output "external_ips" {
  value = {
    for instance in yandex_compute_instance.node :
    instance.name => yandex_compute_instance.node[instance.name].network_interface.0.nat_ip_address
  }
}

resource "local_file" "ansible_hosts" {
  content = templatefile("./template/hosts.tpl", {
    nodes = {
      for key, values in local.virtual_machines : 
        key => {
          ip = yandex_compute_instance.node[key].network_interface.0.nat_ip_address
          user_name = values.user_name
          ssh_key_path = values.ssh_key_path
        }
    }
  })
  filename = "../hosts"
}
