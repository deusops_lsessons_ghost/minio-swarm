#============================= Yandex Cloud variables ==============================#

variable "zone" {
  description = "Deafult zone"
  default     = "ru-central1-a"
}

variable "yandex-token" {
  description = "Yandex token"
}

variable "yandex-cloud-id" {
  description = "Yandex cloud id"
}

variable "yandex-folder-id" {
  description = "Yandex folder id"
}

variable "yandex-service-account-id" {
  description = "Yandex service account id"
}

#=============================== Instances variables ==================================#

variable "instance-platform-id" {
  description = "Type of instance CPUs"
  type        = string
  default     = "standard-v3"
}

variable "instance-cores" {
  description = "Amount of CPU cores"
  type        = number
  default     = 2
}

variable "instance-memory" {
  description = "Amount of memory, GB"
  type        = number
  default     = 1
}

variable "instance-core-fraction" {
  description = "Fraction per core, %"
  type        = number
  default     = 20
}

variable "instance-image-id" {
  # Ubuntu 22.04 LTS => "fd8egv6phshj1f64q94n"
  # Ubuntu 20.04 LTS => "fd8kdq6d0p8sij7h5qe3"
  description = "Image for instances, Ubuntu LTS"
  type        = string
  default     = "fd8egv6phshj1f64q94n"
}

variable "instance-disk-type" {
  description = "Type of disk (HDD, SSD)"
  type        = string
  default     = "network-hdd"
}

variable "instance-disk-size" {
  description = "Size of disk, GB"
  type        = number
  default     = 15
}

variable "is-auto-shutdown" {
  description = "Is 24h autoshutdown true"
  type        = bool
  default     = true
}

variable "instance-user-template" {
  description = "Creat new user on each instance"
  default     = "./template/ssh_user.tpl"
}

#--------------------- Swarm workers ---------------------#

variable "worker-user" {
  description = "Worker node user name"
  type        = string
  default     = "ghost_w"
}

variable "worker-ssh-key-path" {
  description = "ssh public key path for worker node"
  type        = string
  default     = "~/.ssh/worker_rsa"
}

variable "worker-nodes-amount" {
  description = "Amount of worker nodes"
  type        = number
  default     = 3
}
#--------------------- Swarm master ---------------------#

variable "master-user" {
  description = "Master node user name"
  type        = string
  default     = "ghost_m"
}

variable "master-ssh-key-path" {
  description = "ssh public key path for master node"
  type        = string
  default     = "~/.ssh/master_rsa"
}

variable "master-memory" {
  description = "Amount of memory, GB"
  type        = number
  default     = 1
}
