locals {
  virtual_machines = merge([ 
    for count in range(var.worker-nodes-amount) : {
      "worker${count + 1}" = {
        user_name    = var.worker-user
        ssh_key_path = var.worker-ssh-key-path
        memory       = var.instance-memory
        external_ip  = true
      },
    "master" = {
      memory       = var.master-memory
      user_name    = var.master-user
      ssh_key_path = var.master-ssh-key-path
      external_ip  = true
    }
    }
  ]...)
}

data "template_file" "user_config" {
  for_each    = local.virtual_machines
  template    = file(var.instance-user-template)
  vars = {
    ssh_user  = each.value.user_name
    ssh_key   = file(join("", [each.value.ssh_key_path, ".pub"]))
  }
}

resource "yandex_compute_instance" "node" {
  for_each        = local.virtual_machines
  name            = each.key
  hostname        = each.key
  description     = "${each.key} instance"

  platform_id               = var.instance-platform-id
  allow_stopping_for_update = true

  resources {
    cores         = var.instance-cores
    memory        = try(each.value.memory, var.instance-memory)
    core_fraction = var.instance-core-fraction
  }

  boot_disk {
    initialize_params {
      image_id    = var.instance-image-id
      type        = var.instance-disk-type
      size        = try(each.value.disk-size, var.instance-disk-size)
    }
  }

  network_interface {
    subnet_id     = yandex_vpc_subnet.subnet_terraform.id
    nat           = try(each.value.external_ip, false)
  }

  scheduling_policy {
    preemptible   = var.is-auto-shutdown
  }

  metadata = {
    user-data = data.template_file.user_config[each.key].rendered
  }
}
