%{ for key, values in nodes ~}
%{ if key == "master" ~}
[masters]
${key} ansible_host=${values.ip} ansible_user=${values.user_name} ansible_ssh_private_key_file=${values.ssh_key_path}

[workers]
%{ else ~}
${key} ansible_host=${values.ip} ansible_user=${values.user_name} ansible_ssh_private_key_file=${values.ssh_key_path}
%{ endif ~}
%{ endfor }
