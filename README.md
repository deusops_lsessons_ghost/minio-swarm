# Установка s3 minio в docker swarm

Вополнен полнстью Normal (6 пунктов)

## Инфраструктура

VM для проекта разворачиваются с помощью terraform. Для этого необходимо заполнить [terraform.tfvars.example](https://gitlab.com/deusops_lsessons_ghost/minio-swarm/-/blob/master/terraform/terraform.tfvars.example), переименовать его => **terraform.tfvars** и [backend.conf.example](https://gitlab.com/deusops_lsessons_ghost/minio-swarm/-/blob/master/terraform/backend.conf.example), переименовать в => **backend.conf**, настройки можно изменить в [00-vars.tf](https://gitlab.com/deusops_lsessons_ghost/minio-swarm/-/blob/master/terraform/00-vars.tf) и выполнить последовательно две команды:
```
$ terraform init -backend-config=backend.conf

$ terraform apply -auto-approve
```
Далее нужно скачать роли для ansible и запустить [playbook](https://gitlab.com/deusops_lsessons_ghost/minio-swarm/-/blob/master/playbook.yml) (файл hosts для ansible генерируется с помощью terraform):
```
$ ansible-galaxy install -r requirements.yml

$ ansible-playbook playbook.yml
```
Роли лежат [отдельно](https://gitlab.com/ghosta_projects/ansible_roles). Все роли протестированы с помощью molecule 4.0.4 (пакеты в docker image подбирал максимально свежие на начало 2023)


## minio-swarm

Для работы роли хосты должны быть разделены на две группы, (переопределить названия групп можно в настройках):   
| Название                    | Значение по умолчанию | Описание                                    |
|-----------------------------|-----------------------|---------------------------------------------|
| `swarm_master_port`         | `2377`                | Порт по умолчанию для docker swarm          |
| `swarm_master_group_name`   | `masters`             | Имя группы manager хостов для docker swarm, если больше одного,то все они будут swarm manager. |
| `swarm_worker_group_name`   | `workers`             | Имя группы worker хостов для docker swarm. |
| `swarm_install_visualizer`  | `true`                | Утилита отслеживающая контейнеры в docker-swarm устанавливается на первый инстанс из мастер-группы. |  
| `minio_swarm_nodes`         | `[]`                  | Cписок хостов на которые установится minio (нельзя исключить первый хост из мастер-группы, например если надо исключить несколько воркеров), если список пустой, то minio устанавливается на все хосты. |
| `minio_dir`                 | `/opt/minio`          | Рабочая директория для minio. |

## Зависимости

При установке роли подтягивается роль docker.

## Пример плейбука:

    - hosts: all
      become: true
      roles:
         - mimio-swarm
     
В папке [screenshots](https://gitlab.com/deusops_lsessons_ghost/minio-swarm/-/tree/master/screenshots) лежат скриншоты работы **visualizer** и **minio** запущенного в четырех экземплярах.
